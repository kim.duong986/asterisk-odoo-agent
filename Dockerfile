FROM alpine:3.9
# Stick to 3.9 as python 3.7 has SSL bug.

RUN apk --no-cache add bash tini py3-pip py3-greenlet openssl ipset rabbitmq-c-utils

ARG AGENT_VERSION
ARG SYSTEM_NAME=asterisk

ENV SYSTEM_NAME=${SYSTEM_NAME}

ARG RABBITMQ=127.0.0.1:5672
ENV RABBITMQ=${RABBITMQ}

COPY . /usr/local/src/asterisk-odoo-agent
RUN pip3 install /usr/local/src/asterisk-odoo-agent && rm -rf /usr/local/src/asterisk-odoo-agent

COPY ./deploy/config.yml /app/
COPY ./deploy/docker-entrypoint.sh /docker-entrypoint.sh
COPY ./deploy/setup_security.sh /usr/local/bin/
COPY ./deploy/setup_queuelog.py /usr/local/bin/

WORKDIR /app/

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["/docker-entrypoint.sh"]
