import logging
from nameko.rpc import rpc
from nameko.events import EventDispatcher
from nameko.dependency_providers import Config
from nameko_ari import AriClient, stasis
import os

logger = logging.getLogger(__name__)


SYSTEM_NAME = os.getenv('SYSTEM_NAME', 'asterisk')


class AriService:
    name = '{}_ari'.format(SYSTEM_NAME)
    config = Config()
    ari_client = AriClient()
    dispatch = EventDispatcher()

    @rpc
    def ping(self):
        return 'pong'

    @rpc
    def request(self, model, method, args, raise_for_status=True):
        resource = getattr(self.ari_client, model)
        method = getattr(resource, method)
        res = method(**args)
        if raise_for_status:
            res.raise_for_status()
        return {'status_code': res.status_code,
                'reason': res.reason,
                'text': res.text}

    @rpc
    def originate(self, endpoint=None, timeout=30, callerId="Anonymous <>",
                  context=None, extension=None, priority='1',
                  label=None, return_on='setup'):
        try:
            self.ari_client.channels.originate(
                endpoint=endpoint, app=self.config['ASTERISK_ARI_APP'],
                timeout=timeout, callerId=callerId)
        except Exception as e:
            logger.exception('Error')
            raise

    @stasis
    def on_stasis_event(self, event):
        if self.config.get('ASTERISK_ARI_TRACE_EVENTS'):
            logger.debug('ARI Event: %s', event)
        try:
            self.dispatch(event['type'], event)
        except Exception:
            logger.exception('Dispatch error:')

