import eventlet
eventlet.monkey_patch() 
import ipaddress
import json
import logging
import os
import re
import subprocess
from nameko.events import event_handler
from nameko.dependency_providers import Config
from nameko.rpc import rpc
from ipsetpy import ipset_list, ipset_create_set, ipset_add_entry
from ipsetpy import ipset_del_entry, ipset_test_entry, ipset_flush_set
from ipsetpy.exceptions import IpsetError, IpsetNoRights
from .dependencies.start import on_start


logger = logging.getLogger(__name__)


SYSTEM_NAME = os.getenv('SYSTEM_NAME', 'asterisk')


# ipset list member line example:
# 10.18.0.100 timeout 0 packets 0 bytes 0 comment "Admin added"
RE_IPSET_ENTRY = re.compile(
    r'^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:/\d{1,2})?) '
    'timeout ([0-9]+) packets ([0-9]+) bytes ([0-9]+) comment "(.+)"$')


class SecurityService:
    name = '{}_security'.format(SYSTEM_NAME)
    config = Config()

    @rpc
    def ping(self):
        return 'pong'

    @on_start
    def security_start(self):
        logger.debug('Security start.')
        # TODO: Check ipset & iptables

    @event_handler(SYSTEM_NAME + '_ami', 'InvalidAccountID')
    def on_invalid_account_id(self, event):
        logger.debug('Event InvalidAccountID')
        self.ban_event(event)

    @event_handler(SYSTEM_NAME + '_ami', 'FailedACL')
    def on_failed_acl(self, event):
        logger.warning('Event FailedACL: %s', event)
        if self.config.get('SECURITY_BAN_FAILED_ACL'):
            self.ban_event(event)

    @event_handler(SYSTEM_NAME + '_ami', 'InvalidPassword')
    def on_invalid_password(self, event):
        logger.debug('Event Password')
        self.ban_event(event)

    @event_handler(SYSTEM_NAME + '_ami', 'ChallengeResponseFailed')
    def on_channenge_response_failed(self, event):
        logger.debug('Event Challenge Response Failed')
        self.ban_event(event)

    def ban_event(self, event):
        reason = event['Event']
        if event.get('ACLName'):
            reason = '{} ({})'.format(reason, event.get('ACLName'))
        account = event['AccountID']
        address = event['RemoteAddress']
        service = event['Service']
        address_parts = address.split('/')
        logger.debug(
            f'InvalidAccountOrPassword account {service}/{account}, '
            f'address {address}')
        # Check format RemoteAddress='IPV4/UDP/10.18.0.1/35060'
        if len(address_parts) != 4:
            logger.error(f'Security event parse address error: {event}')
            return False
        ip = address_parts[2]
        return self.ban_ip(ip, service=service, account=account, reason=reason)

    def ban_ip(self, ip, timeout=None, service='', account='', reason=''):
        # Check if this address is not in white list
        if ipset_test_entry('whitelist', ip):
            logger.info(f'{service} {account} {reason} - whitelisted (no ban)')
        else:
            logger.info('IP %s put in blacklist', ip)
            subprocess.check_output(
                f'ipset add blacklist {ip} --exist --comment "{service} {account} {reason}"',
                shell=True)
        return True

    @rpc
    def get_banned(self):
        result = []
        data = ipset_list('blacklist')
        lines = data.split('\n')
        for line in lines:
            # Try IP style
            found = RE_IPSET_ENTRY.search(line)
            if found:
                address, timeout, packets, bytes, comment = found.group(1), \
                    found.group(2), found.group(3), \
                    found.group(4), found.group(5)
                result.append({
                    'address': found.group(1),
                    'timeout': int(found.group(2)),
                    'packets': int(found.group(3)),
                    'bytes': int(found.group(4)),
                    'comment': found.group(5)
                })
        logger.debug('Banned entries: {}'.format(json.dumps(
            ['{}: {}'.format(k['address'], k['comment']) for k in result],
            indent=2)))
        return result

    @rpc
    def remove_banned_address(self, address):
        logger.info('Remove ban address %s', address)
        return ipset_del_entry('blacklist', address, exist=True)

    @rpc
    def remove_banned_addresses(self, address_list):
        for address in address_list:
            self.remove_banned_address(address)
        return True

    @rpc
    def update_access_rules(self, rules):
        # Destroy ipset lists as it will be re-filled.
        ipset_flush_set('whitelist')
        ipset_flush_set('blacklist')
        errors = []
        # Add new rules
        for rule in rules:
            try:
                ip_netmask = rule['address'] if rule['address_type'] == 'ip' \
                    else '{}/{}'.format(
                        rule['address'],
                        str(ipaddress.IPv4Network(
                            rule['address'] + '/' + rule['netmask'])).split(
                            '/')[1])
            except (ipaddress.NetmaskValueError,
                    ipaddress.AddressValueError) as e:
                error_message = ('Cannot convert netmask {} for '
                                 'address: {}').format(rule['netmask'],
                                                       rule['address'])
                logger.error(error_message)
                errors.append(error_message)
                continue
            if rule['access_type'] == 'deny':
                logger.info('Adding {} to blacklist ipset'.format(ip_netmask))
                try:
                    comment = rule['comment'] or 'Admin added'
                    subprocess.check_output(
                        f'ipset add blacklist {ip_netmask} '
                        '--exist '
                        '--timeout 0 '
                        f'--comment "{comment}"', shell=True)
                except subprocess.CalledProcessError as e:
                    logger.error(f'ipset add error {e}')
            elif rule['access_type'] == 'allow':
                logger.info('Adding {} to whitelist ipset'.format(ip_netmask))
                ipset_add_entry('whitelist', ip_netmask, exist=True)
        if errors:
            raise Exception('\n'.join(errors))
