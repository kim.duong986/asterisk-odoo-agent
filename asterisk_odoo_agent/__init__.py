import logging

logger = logging.getLogger(__name__)

__version__ = '1.16'
logger.info('Asterisk Odoo Agent version %s loading...', __version__)

from .ami import AmiService
from .file import FileService
from .odoo import OdooService
from .queue_log import QueueLogService
from .security import SecurityService
