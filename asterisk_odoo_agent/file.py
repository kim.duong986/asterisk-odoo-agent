import base64
import logging
import os
from nameko.dependency_providers import Config
from nameko.rpc import rpc

logger = logging.getLogger(__name__)

SYSTEM_NAME = os.getenv('SYSTEM_NAME', 'asterisk')


class FileService:
    name = '{}_files'.format(SYSTEM_NAME)
    config = Config()

    @rpc
    def ping(self):
        return 'pong'

    @rpc
    def put_file(self, path, data):
        logger.info('Put file %s.', path)
        open(path, 'wb').write(base64.b64decode(data.encode()))
        return True

    @rpc
    def delete_file(self, path):
        logger.info('Delete fle %s.', path)
        os.unlink(path)
        return True

    @rpc
    def get_file(self, path):
        logger.info('Get file %s.', path)
        file_data = open(path, 'rb').read()
        return {
            'file_data': base64.b64encode(file_data).decode(),
        }

    # Configuration files management
    def _get_etc_dir(self):
        return self.config.get('ASTERISK_ETC_DIR', '/etc/asterisk/')

    @rpc
    def get_config(self, file):
        res = self.get_file(os.path.join(self._get_etc_dir(), file))
        return res

    @rpc
    def put_config(self, file, data):
        res = self.put_file(
            os.path.join(self._get_etc_dir(), file), data)
        return res

    @rpc
    def delete_config(self, files):
        if type(files) is not list:
            files = list(files)
        for file in files:
            self.delete_file(os.path.join(self._get_etc_dir(), file))
        return True

    @rpc
    def get_all_configs(self):
        files = {}
        etc_dir = self._get_etc_dir()
        for file in [k for k in os.listdir(
                etc_dir) if os.path.isfile(
                os.path.join(etc_dir, k)) and k.endswith('.conf')]:
            files[file] = self.get_file(os.path.join(etc_dir, file))
        logger.info('Get %s configs.', len(files.keys()))
        return files

    @rpc
    def put_all_configs(self, data):
        etc_dir = self._get_etc_dir()
        for filename, filedata in data.items():
            self.put_file(os.path.join(etc_dir, filename), filedata)
        logger.info('Put %s configs.', len(data.keys()))
        return True

    # Voice prompts management
    def _get_odoo_sounds_dir(self):
        data_dir = self.config.get('ASTERISK_DATA_DIR', '/var/lib/asterisk/')
        odoo_sounds_dir = os.path.join(data_dir, 'sounds', 'odoo')
        if not os.path.isdir(odoo_sounds_dir):
            os.mkdir(odoo_sounds_dir)
        return odoo_sounds_dir

    @rpc
    def get_prompt(self, file):
        return self.get_file(os.path.join(self._get_odoo_sounds_dir(), file))

    @rpc
    def put_prompt(self, file, data):
        return self.put_file(
            os.path.join(self._get_odoo_sounds_dir(), file), data)

    @rpc
    def delete_prompt(self, file):
        self.delete_file(os.path.join(self._get_odoo_sounds_dir(), file))
        return True
