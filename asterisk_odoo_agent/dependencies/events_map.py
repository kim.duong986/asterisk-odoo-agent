import json
import logging
import os
import yaml
from nameko.extensions import DependencyProvider, SharedExtension

logger = logging.getLogger(__name__)


class EventsMap(DependencyProvider, SharedExtension):
    events_map = []

    def load_events_from_file(self, events_file):
        events_map = []
        try:
            logger.debug('Loading events from %s.', events_file)
            events_map = yaml.safe_load(open(events_file).read())
            logger.info(
                'Loaded %s events from %s.', len(events_map), events_file)
        except FileNotFoundError:
            logger.warning('File %s not found.', events_file)
        except Exception:
            logger.exception(
                'Cannot load events from %s:', events_file)
        finally:
            return events_map

    def setup(self):
        events_files = self.container.config.get('EVENTS_MAP')
        if events_files:
            if type(events_files) == str:
                events_files = [events_files]
            for events_file in events_files:
                # Check if a directory is specified
                if os.path.isdir(events_file):
                    for file in os.listdir(events_file):
                        if file.endswith('.yml'):
                            dir_file = os.path.join(events_file, file)
                            self.events_map.extend(
                                self.load_events_from_file(dir_file))
                else:
                    self.events_map.extend(
                        self.load_events_from_file(events_file))
            logger.debug('Events: %s', json.dumps(self.events_map, indent=2))
        else:
            logger.debug('Events map not defined.')

    def get_dependency(self, worker_ctx):
        return self
