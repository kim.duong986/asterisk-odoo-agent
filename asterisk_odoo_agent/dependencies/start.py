import logging
from nameko.extensions import Entrypoint


logger = logging.getLogger(__name__)


class Start(Entrypoint):

    def start(self):
        self.container.spawn_worker(self, (), {})


on_start = Start.decorator
