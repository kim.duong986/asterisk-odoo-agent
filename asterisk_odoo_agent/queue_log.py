import logging
import os
from nameko.rpc import RpcProxy
from .dependencies.queue_log import queue_log

logger = logging.getLogger(__name__)


SYSTEM_NAME = os.getenv('SYSTEM_NAME', 'asterisk')


class QueueLogService(object):
    name = '{}_queue_log'.format(SYSTEM_NAME)
    odoo = RpcProxy('odoo')

    @queue_log
    def queue_log_read(self, data):
        # TODO: Add system_name
        try:
            logger.debug('Pipe read: %s', data)
            names = ['time', 'call_id', 'queue_name', 'agent', 'event_type',
                     'data1', 'data2', 'data3', 'data4', 'data5']

            self.odoo.execute('asterisk_queues.queue_log', 'create',
                              dict(zip(names, data.split('|'))))
        except Exception:
            logger.exception('Collect error:')
