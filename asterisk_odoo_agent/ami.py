import json
import eventlet
eventlet.monkey_patch() 
import logging
import os
from nameko.events import EventDispatcher
from nameko.dependency_providers import Config
from nameko.rpc import rpc
from nameko_ami import AmiClientExtension, ami


logger = logging.getLogger(__name__)


SYSTEM_NAME = os.getenv('SYSTEM_NAME', 'asterisk')


class AmiService:
    name = '{}_ami'.format(SYSTEM_NAME)
    config = Config()
    ami_client = AmiClientExtension()
    dispatch = EventDispatcher()

    @rpc
    def ping(self):
        return {'status': 'pong'}

    @rpc
    def send_action(self, action):
        logger.debug('AMI send action: %s', action)
        result = self.ami_client.manager.send_action(action)
        logger.debug('Action result: %s', result)
        result.headers['response'] = result.response
        return result.headers

    @rpc
    def send_command(self, command):
        result = self.ami_client.manager.command(command)
        return result.headers

    @ami('*')
    def on_ami_event(self, event, manager):
        # Dispatch AMI events on Nameko bus so that any service can get it.
        try:
            # Overwrite Asterisk system name for all messages coming from AMI.
            event['SystemName'] = SYSTEM_NAME
            self.dispatch(event['Event'], event)
            return True
        except Exception:
            logger.exception('Dispatch error:')
