####################
Asterisk Odoo Agent
####################
.. image:: https://gitlab.com/odooist/asterisk-odoo-agent/-/raw/master/logo.png

.. contents:: 

Introduction
++++++++++++

Asterisk Odoo Agent is a middleware software used as a bridge between `Odoo <http://odoo.com>`_ 
and `Asterisk IP-PBX <http://asterisk.org>`_.

Using this solution makes Odoo a phone applications platform as all development is 
done on Odoo level so every Odoo developer can quickly and easily integrate Asterisk
features with any Odoo module.

The Agent architecture is a callback based where every callback is an Odoo method.

In many cases the Agent will just work *as is* without any modification like it works in
`Asterisk Calls <https://apps.odoo.com/apps/modules/13.0/asterisk_calls/>`_ application.

Though Asterisk Odoo Agent was developed as a required component for commercial module 
it was decided to make the Agent open and free under LGPL license so that all the Odoo 
world can benefit from it.

Dependencies
++++++++++++
Asterisk Odoo Agent is based on `Nameko <http://nameko.io>`_ - a wonderful microservice framework for Python.

Nameko uses AMQP broker that should be deployed. RabbitMQ is recommended.

Asterisk Odoo Agent is very small because Odoo related operations come from `nameko-odoo <http://github.com/litnimax/nameko-odoo>`_ library
and Asterisk related operations come from `nameko-ami <https://github.com/litnimax/nameko-ami>`_ library.

Installation
++++++++++++

Using PyPi
----------

.. code:: bash

  pip3 install asterisk-odoo-agent

Docker based
------------
See `Docker HUB <https://hub.docker.com/r/odooist/asterisk-odoo-agent>`_ project page for details.

Configuration
+++++++++++++
Agent
-----
Nameko style config is used.

See `config.yml <https://gitlab.com/odooist/asterisk-odoo-agent/-/blob/master/deploy/config.yml>`_ for a default config.

Also please refer to `nameko-odoo` and `nameko-ami` documentation for deeper understanding of configuration options.

You should override default configuration settings with your own.

Here is the list of settings to check:

* ASTERISK_AMI_HOST: IP address or hostname of your Asterisk server.
* ASTERISK_AMI_PORT: Asterisk manager interface port (default is 5038).
* ASTERISK_AMI_USER: Asterisk manager account from manager.conf.
* ASTERISK_AMI_PASS: Asterisk manager password from manager.conf.
* ASTERISK_AMI_TRACE_EVENTS: set to `yes` if you want to debug all AMI events.
* ODOO_HOST: Odoo server IP address or hostname.
* ODOO_PORT: Odoo port, 8069 for standalone and 80 or 443 when Odoo is behind proxy.
* ODOO_USE_SSL: Set to `yes` if your proxy uses HTTPS.
* ODOO_USER: Odoo system account login that is used by the Agent..
* ODOO_PASS: Odoo system account password that is used by the Agent.
* ODOO_DB: Odoo database.
* ODOO_BUS_ENABLED: By default Remote Agent app uses Odoo bus to communicate with the Agent. It does not work with odoo.sh so you can disable it.
* ODOO_BUS_PORT: Default is 8072. Change to 80 or 443 if your Odoo is behind proxy.
* WEB_SERVER_ENABLED: When Odoo bus polling is disabled internal HTTP server must be started.
* WEB_SERVER_ADDRESS: Default is `0.0.0.0:40000`.  You should assure that this port is accessible from your Odoo instance.
* ODOO_TRACE_AMI_EVENTS: Set to `yes` if you want to debug AMI events sent to Odoo.
 
Asterisk
--------

See `Asterisk Calls module <https://apps.odoo.com/apps/modules/13.0/asterisk_calls#doc>`_ documentation on preparing Asterisk for the Agent.

Running Asterisk Odoo Agent
++++++++++++++++++++++++++++
You can start the Agent manually from any place like this:

.. code:: bash

  nameko run --config=config.yml asterisk_odoo_agent

You can also create a systemd service file ``asterisk_odoo_agent.service`` and 
install it for starting the Agent at boot (adjust it for your environment).

.. code:: bash

  [Unit]
  Description=Asterisk Odoo Odoo connector
  After=network.target

  [Service]
  Environment="SYSTEM_NAME=asterisk"
  User=root
  ExecStart=/usr/local/bin/nameko run --config=/etc/asterisk_odoo_agent.yml asterisk_odoo_agent
  Restart=always
  RemainAfterExit=no
  StandardOutput=syslog
  StandardError=syslog
  SyslogIdentifier=ASTERISK_CALLS_AGENT

  [Install]
  WantedBy=multi-user.target

**And activate it**:

.. code:: bash

  systemctl daemon-reload
  systemctl enable asterisk_odoo_agent
  systemctl start asterisk_odoo_agent
  systemctl status asterisk_odoo_agent

Support & Contributing
++++++++++++++++++++++
Feel free to create new tickets on issues and ideas.

Development
+++++++++++
As was said in the very beginning development is done on Odoo layer.

The Agent will forward to Odoo specified 
`AMI messages <https://wiki.asterisk.org/wiki/display/AST/Asterisk+13+AMI+Events>`_. 

In order to know which AMI messages to forward your should define your 
events map and include it in the config file (EVENTS_MAP).

Let's imagine that we want to collect call details records. In this case you need to
map ``Cdr`` event to Odoo's model and method which will receive it. 
Add the following section to ``events.yml``:

.. code:: yaml

  - name: Cdr
    type: AMI
    model: odoo_asterisk.call
    method: create_cdr


Now in Odoo application named ``odoo_asterisk`` (just an example) create a method 
``create_cdr`` with the following contents:

.. code:: python

    class Call(models.Model):
      _name = 'odoo_asterisk.call'
      _description = 'Call Log'
      
      src = fields.Char()
      dst = fields.Char()
      channel = fields.Char()
      # The rest fields are ommited...
  
      @api.model
      def create_cdr(self, event):
          get = event['headers'].get
          data = {
              'accountcode': get('AccountCode'),
              'src': get('Source'),
              'dst': get('Destination'),
              'dcontext': get('DestinationContext'),
              'clid': get('CallerID'),
              'channel': get('Channel'),
              'dstchannel': get('DestinationChannel'),
              'lastapp': get('LastApplication'),
              'lastdata': get('LastData'),
              'started': get('StartTime') or False,
              'answered': get('AnswerTime') or False,
              'ended': get('EndTime') or False,
              'duration': get('Duration'),
              'billsec': get('BillableSeconds'),
              'disposition': get('Disposition'),
              'amaflags': get('AMAFlags'),
              'uniqueid': get('UniqueID') or get('Uniqueid'),
              'linkedid': get('Linkedid'),
              'userfield': get('UserField'),
              'system_name': get('SystemName'),
          }
          self.create(data)
          return True

That's it.
