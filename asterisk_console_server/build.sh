#!/bin/sh

set -e

VERSION=latest

docker buildx build --platform linux/amd64,linux/arm/v7 --push -t odooist/asterisk-console-server:$VERSION .

